﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName ="Letter", menuName ="Letter",order =1)]
public class Letter : ScriptableObject
{
    public char l;
    public char k;
    public bool withShift;
}


[CreateAssetMenu(fileName = "LetterSet", menuName = "LetterSet", order = 2)]
public class LetterSet : ScriptableObject
{
    public Letter[] letters;
}