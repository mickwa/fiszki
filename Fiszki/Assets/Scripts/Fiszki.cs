﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;

public class Fiszki : MonoBehaviour
{
    [SerializeField] TMP_Text debugText;
    [SerializeField] TMP_Text leftText;
    [SerializeField] TMP_Text rightText;
    string translationsFilePath = Application.streamingAssetsPath + "/Fiszki.txt"; 
    List<string> translations = new List<string>();
    bool reverseOrder = false;

    public void ToggleOrder_UI()
    {
        reverseOrder = !reverseOrder;
    }

    public void SetNextWord_UI()
    {
        var t = GetRandomTranslationPair();
        leftText.text = t.left;
        rightText.text = t.right;
        rightText.gameObject.SetActive(false);
    }

    public void ShowAnswer_UI()
    {
        rightText.gameObject.SetActive(true);
    }
    void Start()
    {
        CheckIfFileExists();
        LoadTranslations();
        SetNextWord_UI();
    }

   void CheckIfFileExists()
    {
        if (File.Exists(translationsFilePath) == false)
            File.WriteAllLines(translationsFilePath, new string[] { "raz ; tiempo" });
    }

    void LoadTranslations()
    {
        foreach(string line in File.ReadAllLines(translationsFilePath))
        {
            if (line.Contains(";") || line.Contains("-"))
                translations.Add(line);
        }
    }

    (string left, string right) GetRandomTranslationPair()
    {
        string line = translations[Random.Range(0, translations.Count)];
        string[] cutLine = { "raz ; tiempo" };
            if (line.Contains(";"))
            cutLine = line.Split(';');
        if (line.Contains("-"))
            cutLine = line.Split('-');

        return reverseOrder ? (cutLine[1], cutLine[0]) : (cutLine[0], cutLine[1]);
    }
}
