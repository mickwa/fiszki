﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.InputSystem;

public class KeyboardPractice : MonoBehaviour
{
    public LetterSet letterSet;
    Letter currentPair;
    int currentIndex = -1;
    [SerializeField] TMP_Text letterText;
    void SetNextPair()
    {
        int nextIndex;
        Debug.Log("Setting next pair");
        do
        {
            nextIndex = UnityEngine.Random.Range(0, letterSet.letters.Length);
        } while (currentIndex == nextIndex);
        currentIndex = nextIndex;
        currentPair = letterSet.letters[currentIndex];
        letterText.text = currentPair.k.ToString();
    }
    Controls inputActions;
    private void Start()
    {
        Keyboard.current.onTextInput += Current_onTextInput;
        inputActions = new Controls();
        inputActions.Actions.SelectKey.performed += SelectKey_performed;
        inputActions.Actions.Enable();
        SetNextPair(); 
    }

    private void SelectKey_performed(InputAction.CallbackContext obj)
    {
        
    }

    private void Current_onTextInput(char pressed)
    {
       
        if (pressed == currentPair.l && Keyboard.current.leftShiftKey.isPressed == currentPair.withShift )
        {
            Debug.Log("Correct :)");
            SetNextPair();
        }
        else
            Debug.Log("Try again :)");

    }

}
